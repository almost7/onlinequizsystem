import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { OqsTestModule } from '../../../test.module';
import { UserQuizzDeleteDialogComponent } from 'app/entities/user-quizz/user-quizz-delete-dialog.component';
import { UserQuizzService } from 'app/entities/user-quizz/user-quizz.service';

describe('Component Tests', () => {
  describe('UserQuizz Management Delete Component', () => {
    let comp: UserQuizzDeleteDialogComponent;
    let fixture: ComponentFixture<UserQuizzDeleteDialogComponent>;
    let service: UserQuizzService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [OqsTestModule],
        declarations: [UserQuizzDeleteDialogComponent]
      })
        .overrideTemplate(UserQuizzDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(UserQuizzDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(UserQuizzService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
