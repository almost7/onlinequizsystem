import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { OqsTestModule } from '../../../test.module';
import { UserQuizzDetailComponent } from 'app/entities/user-quizz/user-quizz-detail.component';
import { UserQuizz } from 'app/shared/model/user-quizz.model';

describe('Component Tests', () => {
  describe('UserQuizz Management Detail Component', () => {
    let comp: UserQuizzDetailComponent;
    let fixture: ComponentFixture<UserQuizzDetailComponent>;
    const route = ({ data: of({ userQuizz: new UserQuizz(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [OqsTestModule],
        declarations: [UserQuizzDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(UserQuizzDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(UserQuizzDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.userQuizz).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
