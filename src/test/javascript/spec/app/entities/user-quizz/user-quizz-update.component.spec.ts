import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { OqsTestModule } from '../../../test.module';
import { UserQuizzUpdateComponent } from 'app/entities/user-quizz/user-quizz-update.component';
import { UserQuizzService } from 'app/entities/user-quizz/user-quizz.service';
import { UserQuizz } from 'app/shared/model/user-quizz.model';

describe('Component Tests', () => {
  describe('UserQuizz Management Update Component', () => {
    let comp: UserQuizzUpdateComponent;
    let fixture: ComponentFixture<UserQuizzUpdateComponent>;
    let service: UserQuizzService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [OqsTestModule],
        declarations: [UserQuizzUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(UserQuizzUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(UserQuizzUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(UserQuizzService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new UserQuizz(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new UserQuizz();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
