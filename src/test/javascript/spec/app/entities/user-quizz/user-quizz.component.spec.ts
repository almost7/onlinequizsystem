import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { OqsTestModule } from '../../../test.module';
import { UserQuizzComponent } from 'app/entities/user-quizz/user-quizz.component';
import { UserQuizzService } from 'app/entities/user-quizz/user-quizz.service';
import { UserQuizz } from 'app/shared/model/user-quizz.model';

describe('Component Tests', () => {
  describe('UserQuizz Management Component', () => {
    let comp: UserQuizzComponent;
    let fixture: ComponentFixture<UserQuizzComponent>;
    let service: UserQuizzService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [OqsTestModule],
        declarations: [UserQuizzComponent],
        providers: []
      })
        .overrideTemplate(UserQuizzComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(UserQuizzComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(UserQuizzService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new UserQuizz(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.userQuizzes[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
