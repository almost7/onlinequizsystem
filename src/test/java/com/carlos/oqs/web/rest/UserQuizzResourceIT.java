package com.carlos.oqs.web.rest;

import com.carlos.oqs.OqsApp;
import com.carlos.oqs.domain.UserQuizz;
import com.carlos.oqs.repository.UserQuizzRepository;
import com.carlos.oqs.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.carlos.oqs.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link UserQuizzResource} REST controller.
 */
@SpringBootTest(classes = OqsApp.class)
public class UserQuizzResourceIT {

    private static final Integer DEFAULT_GRADE = 1;
    private static final Integer UPDATED_GRADE = 2;

    @Autowired
    private UserQuizzRepository userQuizzRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restUserQuizzMockMvc;

    private UserQuizz userQuizz;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UserQuizzResource userQuizzResource = new UserQuizzResource(userQuizzRepository);
        this.restUserQuizzMockMvc = MockMvcBuilders.standaloneSetup(userQuizzResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserQuizz createEntity(EntityManager em) {
        UserQuizz userQuizz = new UserQuizz()
            .grade(DEFAULT_GRADE);
        return userQuizz;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserQuizz createUpdatedEntity(EntityManager em) {
        UserQuizz userQuizz = new UserQuizz()
            .grade(UPDATED_GRADE);
        return userQuizz;
    }

    @BeforeEach
    public void initTest() {
        userQuizz = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserQuizz() throws Exception {
        int databaseSizeBeforeCreate = userQuizzRepository.findAll().size();

        // Create the UserQuizz
        restUserQuizzMockMvc.perform(post("/api/user-quizzes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userQuizz)))
            .andExpect(status().isCreated());

        // Validate the UserQuizz in the database
        List<UserQuizz> userQuizzList = userQuizzRepository.findAll();
        assertThat(userQuizzList).hasSize(databaseSizeBeforeCreate + 1);
        UserQuizz testUserQuizz = userQuizzList.get(userQuizzList.size() - 1);
        assertThat(testUserQuizz.getGrade()).isEqualTo(DEFAULT_GRADE);
    }

    @Test
    @Transactional
    public void createUserQuizzWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userQuizzRepository.findAll().size();

        // Create the UserQuizz with an existing ID
        userQuizz.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserQuizzMockMvc.perform(post("/api/user-quizzes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userQuizz)))
            .andExpect(status().isBadRequest());

        // Validate the UserQuizz in the database
        List<UserQuizz> userQuizzList = userQuizzRepository.findAll();
        assertThat(userQuizzList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllUserQuizzes() throws Exception {
        // Initialize the database
        userQuizzRepository.saveAndFlush(userQuizz);

        // Get all the userQuizzList
        restUserQuizzMockMvc.perform(get("/api/user-quizzes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userQuizz.getId().intValue())))
            .andExpect(jsonPath("$.[*].grade").value(hasItem(DEFAULT_GRADE)));
    }
    
    @Test
    @Transactional
    public void getUserQuizz() throws Exception {
        // Initialize the database
        userQuizzRepository.saveAndFlush(userQuizz);

        // Get the userQuizz
        restUserQuizzMockMvc.perform(get("/api/user-quizzes/{id}", userQuizz.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userQuizz.getId().intValue()))
            .andExpect(jsonPath("$.grade").value(DEFAULT_GRADE));
    }

    @Test
    @Transactional
    public void getNonExistingUserQuizz() throws Exception {
        // Get the userQuizz
        restUserQuizzMockMvc.perform(get("/api/user-quizzes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserQuizz() throws Exception {
        // Initialize the database
        userQuizzRepository.saveAndFlush(userQuizz);

        int databaseSizeBeforeUpdate = userQuizzRepository.findAll().size();

        // Update the userQuizz
        UserQuizz updatedUserQuizz = userQuizzRepository.findById(userQuizz.getId()).get();
        // Disconnect from session so that the updates on updatedUserQuizz are not directly saved in db
        em.detach(updatedUserQuizz);
        updatedUserQuizz
            .grade(UPDATED_GRADE);

        restUserQuizzMockMvc.perform(put("/api/user-quizzes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedUserQuizz)))
            .andExpect(status().isOk());

        // Validate the UserQuizz in the database
        List<UserQuizz> userQuizzList = userQuizzRepository.findAll();
        assertThat(userQuizzList).hasSize(databaseSizeBeforeUpdate);
        UserQuizz testUserQuizz = userQuizzList.get(userQuizzList.size() - 1);
        assertThat(testUserQuizz.getGrade()).isEqualTo(UPDATED_GRADE);
    }

    @Test
    @Transactional
    public void updateNonExistingUserQuizz() throws Exception {
        int databaseSizeBeforeUpdate = userQuizzRepository.findAll().size();

        // Create the UserQuizz

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserQuizzMockMvc.perform(put("/api/user-quizzes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userQuizz)))
            .andExpect(status().isBadRequest());

        // Validate the UserQuizz in the database
        List<UserQuizz> userQuizzList = userQuizzRepository.findAll();
        assertThat(userQuizzList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUserQuizz() throws Exception {
        // Initialize the database
        userQuizzRepository.saveAndFlush(userQuizz);

        int databaseSizeBeforeDelete = userQuizzRepository.findAll().size();

        // Delete the userQuizz
        restUserQuizzMockMvc.perform(delete("/api/user-quizzes/{id}", userQuizz.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UserQuizz> userQuizzList = userQuizzRepository.findAll();
        assertThat(userQuizzList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
