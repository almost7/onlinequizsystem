package com.carlos.oqs.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.carlos.oqs.web.rest.TestUtil;

public class UserQuizzTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserQuizz.class);
        UserQuizz userQuizz1 = new UserQuizz();
        userQuizz1.setId(1L);
        UserQuizz userQuizz2 = new UserQuizz();
        userQuizz2.setId(userQuizz1.getId());
        assertThat(userQuizz1).isEqualTo(userQuizz2);
        userQuizz2.setId(2L);
        assertThat(userQuizz1).isNotEqualTo(userQuizz2);
        userQuizz1.setId(null);
        assertThat(userQuizz1).isNotEqualTo(userQuizz2);
    }
}
