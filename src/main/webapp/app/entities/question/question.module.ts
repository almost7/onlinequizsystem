import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { OqsSharedModule } from 'app/shared/shared.module';
import { QuestionComponent } from './question.component';
import { QuestionDetailComponent } from './question-detail.component';
import { QuestionUpdateComponent } from './question-update.component';
import { QuestionDeletePopupComponent, QuestionDeleteDialogComponent } from './question-delete-dialog.component';
import { questionRoute, questionPopupRoute } from './question.route';

const ENTITY_STATES = [...questionRoute, ...questionPopupRoute];

@NgModule({
  imports: [OqsSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    QuestionComponent,
    QuestionDetailComponent,
    QuestionUpdateComponent,
    QuestionDeleteDialogComponent,
    QuestionDeletePopupComponent
  ],
  entryComponents: [QuestionDeleteDialogComponent]
})
export class OqsQuestionModule {}
