import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { OqsSharedModule } from 'app/shared/shared.module';
import { UserQuizzComponent } from './user-quizz.component';
import { UserQuizzDetailComponent } from './user-quizz-detail.component';
import { UserQuizzUpdateComponent } from './user-quizz-update.component';
import { UserQuizzDeletePopupComponent, UserQuizzDeleteDialogComponent } from './user-quizz-delete-dialog.component';
import { userQuizzRoute, userQuizzPopupRoute } from './user-quizz.route';

const ENTITY_STATES = [...userQuizzRoute, ...userQuizzPopupRoute];

@NgModule({
  imports: [OqsSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    UserQuizzComponent,
    UserQuizzDetailComponent,
    UserQuizzUpdateComponent,
    UserQuizzDeleteDialogComponent,
    UserQuizzDeletePopupComponent
  ],
  entryComponents: [UserQuizzDeleteDialogComponent]
})
export class OqsUserQuizzModule {}
