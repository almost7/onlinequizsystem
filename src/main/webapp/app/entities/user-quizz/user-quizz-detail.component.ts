import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IUserQuizz } from 'app/shared/model/user-quizz.model';

@Component({
  selector: 'jhi-user-quizz-detail',
  templateUrl: './user-quizz-detail.component.html'
})
export class UserQuizzDetailComponent implements OnInit {
  userQuizz: IUserQuizz;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ userQuizz }) => {
      this.userQuizz = userQuizz;
    });
  }

  previousState() {
    window.history.back();
  }
}
