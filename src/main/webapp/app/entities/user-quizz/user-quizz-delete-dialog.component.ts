import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IUserQuizz } from 'app/shared/model/user-quizz.model';
import { UserQuizzService } from './user-quizz.service';

@Component({
  selector: 'jhi-user-quizz-delete-dialog',
  templateUrl: './user-quizz-delete-dialog.component.html'
})
export class UserQuizzDeleteDialogComponent {
  userQuizz: IUserQuizz;

  constructor(protected userQuizzService: UserQuizzService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.userQuizzService.delete(id).subscribe(() => {
      this.eventManager.broadcast({
        name: 'userQuizzListModification',
        content: 'Deleted an userQuizz'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-user-quizz-delete-popup',
  template: ''
})
export class UserQuizzDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ userQuizz }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(UserQuizzDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.userQuizz = userQuizz;
        this.ngbModalRef.result.then(
          () => {
            this.router.navigate(['/user-quizz', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          () => {
            this.router.navigate(['/user-quizz', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
