import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { IUserQuizz } from 'app/shared/model/user-quizz.model';
import { UserQuizzService } from './user-quizz.service';

@Component({
  selector: 'jhi-user-quizz',
  templateUrl: './user-quizz.component.html'
})
export class UserQuizzComponent implements OnInit, OnDestroy {
  userQuizzes: IUserQuizz[];
  eventSubscriber: Subscription;

  constructor(protected userQuizzService: UserQuizzService, protected eventManager: JhiEventManager) {}

  loadAll() {
    this.userQuizzService.query().subscribe((res: HttpResponse<IUserQuizz[]>) => {
      this.userQuizzes = res.body;
    });
  }

  ngOnInit() {
    this.loadAll();
    this.registerChangeInUserQuizzes();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IUserQuizz) {
    return item.id;
  }

  registerChangeInUserQuizzes() {
    this.eventSubscriber = this.eventManager.subscribe('userQuizzListModification', () => this.loadAll());
  }
}
