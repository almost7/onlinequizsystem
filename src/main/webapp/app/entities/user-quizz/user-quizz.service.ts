import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IUserQuizz } from 'app/shared/model/user-quizz.model';

type EntityResponseType = HttpResponse<IUserQuizz>;
type EntityArrayResponseType = HttpResponse<IUserQuizz[]>;

@Injectable({ providedIn: 'root' })
export class UserQuizzService {
  public resourceUrl = SERVER_API_URL + 'api/user-quizzes';

  constructor(protected http: HttpClient) {}

  create(userQuizz: IUserQuizz): Observable<EntityResponseType> {
    return this.http.post<IUserQuizz>(this.resourceUrl, userQuizz, { observe: 'response' });
  }

  update(userQuizz: IUserQuizz): Observable<EntityResponseType> {
    return this.http.put<IUserQuizz>(this.resourceUrl, userQuizz, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IUserQuizz>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IUserQuizz[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
