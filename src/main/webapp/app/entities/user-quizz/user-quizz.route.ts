import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserQuizz } from 'app/shared/model/user-quizz.model';
import { UserQuizzService } from './user-quizz.service';
import { UserQuizzComponent } from './user-quizz.component';
import { UserQuizzDetailComponent } from './user-quizz-detail.component';
import { UserQuizzUpdateComponent } from './user-quizz-update.component';
import { UserQuizzDeletePopupComponent } from './user-quizz-delete-dialog.component';
import { IUserQuizz } from 'app/shared/model/user-quizz.model';

@Injectable({ providedIn: 'root' })
export class UserQuizzResolve implements Resolve<IUserQuizz> {
  constructor(private service: UserQuizzService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IUserQuizz> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((userQuizz: HttpResponse<UserQuizz>) => userQuizz.body));
    }
    return of(new UserQuizz());
  }
}

export const userQuizzRoute: Routes = [
  {
    path: '',
    component: UserQuizzComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'UserQuizzes'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: UserQuizzDetailComponent,
    resolve: {
      userQuizz: UserQuizzResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'UserQuizzes'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: UserQuizzUpdateComponent,
    resolve: {
      userQuizz: UserQuizzResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'UserQuizzes'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: UserQuizzUpdateComponent,
    resolve: {
      userQuizz: UserQuizzResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'UserQuizzes'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const userQuizzPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: UserQuizzDeletePopupComponent,
    resolve: {
      userQuizz: UserQuizzResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'UserQuizzes'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
