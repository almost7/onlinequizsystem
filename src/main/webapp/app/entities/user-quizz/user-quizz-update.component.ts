import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';
import { IUserQuizz, UserQuizz } from 'app/shared/model/user-quizz.model';
import { UserQuizzService } from './user-quizz.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { IQuiz } from 'app/shared/model/quiz.model';
import { QuizService } from 'app/entities/quiz/quiz.service';

@Component({
  selector: 'jhi-user-quizz-update',
  templateUrl: './user-quizz-update.component.html'
})
export class UserQuizzUpdateComponent implements OnInit {
  isSaving: boolean;

  users: IUser[];

  quizzes: IQuiz[];

  editForm = this.fb.group({
    id: [],
    grade: [],
    user: [],
    quiz: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected userQuizzService: UserQuizzService,
    protected userService: UserService,
    protected quizService: QuizService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ userQuizz }) => {
      this.updateForm(userQuizz);
    });
    this.userService
      .query()
      .subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body), (res: HttpErrorResponse) => this.onError(res.message));
    this.quizService
      .query()
      .subscribe((res: HttpResponse<IQuiz[]>) => (this.quizzes = res.body), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(userQuizz: IUserQuizz) {
    this.editForm.patchValue({
      id: userQuizz.id,
      grade: userQuizz.grade,
      user: userQuizz.user,
      quiz: userQuizz.quiz
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const userQuizz = this.createFromForm();
    if (userQuizz.id !== undefined) {
      this.subscribeToSaveResponse(this.userQuizzService.update(userQuizz));
    } else {
      this.subscribeToSaveResponse(this.userQuizzService.create(userQuizz));
    }
  }

  private createFromForm(): IUserQuizz {
    return {
      ...new UserQuizz(),
      id: this.editForm.get(['id']).value,
      grade: this.editForm.get(['grade']).value,
      user: this.editForm.get(['user']).value,
      quiz: this.editForm.get(['quiz']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IUserQuizz>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }

  trackQuizById(index: number, item: IQuiz) {
    return item.id;
  }
}
