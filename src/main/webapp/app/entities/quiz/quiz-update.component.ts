import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';
import { IQuiz, Quiz } from 'app/shared/model/quiz.model';
import { QuizService } from './quiz.service';
import { ISubject } from 'app/shared/model/subject.model';
import { SubjectService } from 'app/entities/subject/subject.service';

@Component({
  selector: 'jhi-quiz-update',
  templateUrl: './quiz-update.component.html'
})
export class QuizUpdateComponent implements OnInit {
  isSaving: boolean;

  subjects: ISubject[];

  editForm = this.fb.group({
    id: [],
    quizName: [],
    minGrade: [],
    avgGrade: [],
    subject: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected quizService: QuizService,
    protected subjectService: SubjectService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ quiz }) => {
      this.updateForm(quiz);
    });
    this.subjectService
      .query()
      .subscribe((res: HttpResponse<ISubject[]>) => (this.subjects = res.body), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(quiz: IQuiz) {
    this.editForm.patchValue({
      id: quiz.id,
      quizName: quiz.quizName,
      minGrade: quiz.minGrade,
      avgGrade: quiz.avgGrade,
      subject: quiz.subject
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const quiz = this.createFromForm();
    if (quiz.id !== undefined) {
      this.subscribeToSaveResponse(this.quizService.update(quiz));
    } else {
      this.subscribeToSaveResponse(this.quizService.create(quiz));
    }
  }

  private createFromForm(): IQuiz {
    return {
      ...new Quiz(),
      id: this.editForm.get(['id']).value,
      quizName: this.editForm.get(['quizName']).value,
      minGrade: this.editForm.get(['minGrade']).value,
      avgGrade: this.editForm.get(['avgGrade']).value,
      subject: this.editForm.get(['subject']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IQuiz>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackSubjectById(index: number, item: ISubject) {
    return item.id;
  }
}
