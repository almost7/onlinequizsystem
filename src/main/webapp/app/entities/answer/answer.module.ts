import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { OqsSharedModule } from 'app/shared/shared.module';
import { AnswerComponent } from './answer.component';
import { AnswerDetailComponent } from './answer-detail.component';
import { AnswerUpdateComponent } from './answer-update.component';
import { AnswerDeletePopupComponent, AnswerDeleteDialogComponent } from './answer-delete-dialog.component';
import { answerRoute, answerPopupRoute } from './answer.route';

const ENTITY_STATES = [...answerRoute, ...answerPopupRoute];

@NgModule({
  imports: [OqsSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [AnswerComponent, AnswerDetailComponent, AnswerUpdateComponent, AnswerDeleteDialogComponent, AnswerDeletePopupComponent],
  entryComponents: [AnswerDeleteDialogComponent]
})
export class OqsAnswerModule {}
