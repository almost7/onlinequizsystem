import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'quiz',
        loadChildren: () => import('./quiz/quiz.module').then(m => m.OqsQuizModule)
      },
      {
        path: 'subject',
        loadChildren: () => import('./subject/subject.module').then(m => m.OqsSubjectModule)
      },
      {
        path: 'question',
        loadChildren: () => import('./question/question.module').then(m => m.OqsQuestionModule)
      },
      {
        path: 'answer',
        loadChildren: () => import('./answer/answer.module').then(m => m.OqsAnswerModule)
      },
      {
        path: 'user-quizz',
        loadChildren: () => import('./user-quizz/user-quizz.module').then(m => m.OqsUserQuizzModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class OqsEntityModule {}
