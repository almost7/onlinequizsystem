import { IQuestion } from 'app/shared/model/question.model';

export interface IAnswer {
  id?: number;
  answerName?: string;
  correctAnswer?: boolean;
  question?: IQuestion;
}

export class Answer implements IAnswer {
  constructor(public id?: number, public answerName?: string, public correctAnswer?: boolean, public question?: IQuestion) {
    this.correctAnswer = this.correctAnswer || false;
  }
}
