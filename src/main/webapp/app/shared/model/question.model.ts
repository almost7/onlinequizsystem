import { IAnswer } from 'app/shared/model/answer.model';
import { IQuiz } from 'app/shared/model/quiz.model';

export interface IQuestion {
  id?: number;
  questionTitle?: string;
  questionDescription?: string;
  answers?: IAnswer[];
  quiz?: IQuiz;
}

export class Question implements IQuestion {
  constructor(
    public id?: number,
    public questionTitle?: string,
    public questionDescription?: string,
    public answers?: IAnswer[],
    public quiz?: IQuiz
  ) {}
}
