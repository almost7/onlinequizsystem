import { IUser } from 'app/core/user/user.model';
import { IQuiz } from 'app/shared/model/quiz.model';

export interface IUserQuizz {
  id?: number;
  grade?: number;
  user?: IUser;
  quiz?: IQuiz;
}

export class UserQuizz implements IUserQuizz {
  constructor(public id?: number, public grade?: number, public user?: IUser, public quiz?: IQuiz) {}
}
