import { IQuestion } from 'app/shared/model/question.model';
import { ISubject } from 'app/shared/model/subject.model';

export interface IQuiz {
  id?: number;
  quizName?: string;
  minGrade?: number;
  avgGrade?: number;
  questions?: IQuestion[];
  subject?: ISubject;
}

export class Quiz implements IQuiz {
  constructor(
    public id?: number,
    public quizName?: string,
    public minGrade?: number,
    public avgGrade?: number,
    public questions?: IQuestion[],
    public subject?: ISubject
  ) {}
}
