export interface ISubject {
  id?: number;
  subjectName?: string;
}

export class Subject implements ISubject {
  constructor(public id?: number, public subjectName?: string) {}
}
