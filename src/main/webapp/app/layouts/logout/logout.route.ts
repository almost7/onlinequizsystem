import { Routes } from '@angular/router';

import { LogoutComponent } from './logout.component';

export const logoutRoute: Routes = [
  {
    path: 'logoff',
    component: LogoutComponent
  }
];
