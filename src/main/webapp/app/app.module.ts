import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { OqsSharedModule } from 'app/shared/shared.module';
import { OqsCoreModule } from 'app/core/core.module';
import { OqsAppRoutingModule } from './app-routing.module';
import { OqsHomeModule } from './home/home.module';
import { OqsEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { JhiMainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';
import { OqsLogoutModule } from './layouts/logout/logout.module';

@NgModule({
  imports: [
    BrowserModule,
    OqsSharedModule,
    OqsCoreModule,
    OqsHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    OqsEntityModule,
    OqsLogoutModule,
    OqsAppRoutingModule
  ],
  declarations: [JhiMainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [JhiMainComponent]
})
export class OqsAppModule {}
