package com.carlos.oqs.web.mvc;

import com.carlos.oqs.domain.*;
import com.carlos.oqs.security.SecurityUtils;
import com.carlos.oqs.security.jwt.TokenProvider;
import com.carlos.oqs.service.QuizService;
import com.carlos.oqs.service.SubjectService;
import com.carlos.oqs.service.UserService;
import com.carlos.oqs.service.dto.QuizDTO;
import com.carlos.oqs.service.dto.UserDTO;
import com.carlos.oqs.web.rest.QuizResource;
import com.carlos.oqs.web.rest.UserResource;
import com.carlos.oqs.web.rest.vm.LoginVM;
import com.hazelcast.util.Base64;
import io.swagger.models.Model;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpSession;
import java.io.*;
import java.lang.reflect.Array;
import java.net.URISyntaxException;
import java.util.*;

@Controller
public class HomeController {

    @Autowired
    private UserService service;

    @Autowired
    private QuizService quizService;

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private UserService userService;
    private final TokenProvider tokenProvider;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;

    public HomeController(TokenProvider tokenProvider, AuthenticationManagerBuilder authenticationManagerBuilder, UserService userService){
        this.tokenProvider = tokenProvider;
        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.userService = userService;
    }

    @GetMapping("/oqs")
    public ModelAndView regularAccess(Pageable pageable) throws URISyntaxException {
        ModelAndView mv = new ModelAndView("views/mainAvailableQuizzes");
        String login = "anonymous";
        if(SecurityUtils.getCurrentUserLogin().isPresent()) {
            login = SecurityUtils.getCurrentUserLogin().get();
            Optional<UserDTO> optionalUserDTO = service.getUserWithAuthoritiesByLogin(login).map(UserDTO::new);
            optionalUserDTO.ifPresent(userDTO -> mv.addObject("user", userDTO));
        }
        Page<QuizDTO> newList = quizService.findAllWithQuestions(pageable);
        mv.addObject("quizzes", newList);
        mv.addObject("pages", (newList.getTotalElements()/5)-1);
        List<Integer> grades = quizService.AnsweredQuizGetter(pageable);
        mv.addObject("grades", grades);

        return mv;
    }

    @PostMapping("/oqs")
    public ModelAndView regularPostAccess(Pageable pageable, @RequestParam("search") String search) throws URISyntaxException {
        ModelAndView mv = new ModelAndView("views/mainAvailableQuizzes");
        String login = "anonymous";
        if(SecurityUtils.getCurrentUserLogin().isPresent()) {
            login = SecurityUtils.getCurrentUserLogin().get();
            Optional<UserDTO> optionalUserDTO = service.getUserWithAuthoritiesByLogin(login).map(UserDTO::new);
            optionalUserDTO.ifPresent(userDTO -> mv.addObject("user", userDTO));
        }
        Page<QuizDTO> newList = quizService.findAllBySubjectName(pageable, search);
        mv.addObject("quizzes", newList);
        mv.addObject("pages", (newList.getTotalElements()/5)-1);
        List<Integer> grades = quizService.AnsweredQuizGetter(pageable);
        mv.addObject("grades", grades);
        return mv;
    }

    @GetMapping("/addData")
    public String addAData(QuizDTO quiz) throws URISyntaxException {

        return "homeFile";
    }

    @GetMapping("")
    public ModelAndView firstPageLogin() throws URISyntaxException {
        return new ModelAndView("views/login");
    }

    @PostMapping("")
    public RedirectView makeLogin(LoginVM loginVM, HttpSession session, RedirectAttributes redirectAttributes) throws URISyntaxException {
        try{
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(loginVM.getUsername(), loginVM.getPassword());
            Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = tokenProvider.createToken(authentication, false);
            session.setAttribute("jwt",jwt);
            redirectAttributes.addFlashAttribute("successMessage", "Login with success");
            return new RedirectView("/gateway");
        }
        catch(Exception e){
            redirectAttributes.addFlashAttribute("errorMessage", "Login with errors");
            return new RedirectView("");
        }
    }

    @GetMapping("register")
    public ModelAndView firstPageRegister() throws URISyntaxException {
        return new ModelAndView("views/register");
    }

    @PostMapping("register")
    public RedirectView registerUser(RedirectAttributes redirectAttributes, @RequestParam MultiValueMap<String, String> formData, @RequestParam("file") MultipartFile file) throws URISyntaxException, IOException {
        Map<String,String> singleFormData = formData.toSingleValueMap();
        if(!userService.checkEmailAndLogin(singleFormData.get("email"), singleFormData.get("login")) || !Objects.requireNonNull(file.getContentType()).contains("image")){
            redirectAttributes.addFlashAttribute("errorMessage", "the username or email provided already exists (or user pic is not valid)");
            RedirectView mv = new RedirectView("/register");
            redirectAttributes.addFlashAttribute("login", singleFormData.get("login"));
            redirectAttributes.addFlashAttribute("firstName", singleFormData.get("firstName"));
            redirectAttributes.addFlashAttribute("lastName", singleFormData.get("lastName"));
            redirectAttributes.addFlashAttribute("email", singleFormData.get("email"));
            return mv;
        }
        byte[] content = file.getBytes();
        String encodedString = java.util.Base64.getEncoder().encodeToString(content);
        userService.startRegistration(singleFormData, encodedString);
        redirectAttributes.addFlashAttribute("successMessage", "you've registered with success, login to access your account");
        return new RedirectView("/login");
    }

    @PostMapping("/fileUpload")
    public ResponseEntity<Object> fileUpload(@RequestParam("file") MultipartFile file) throws IOException {
        if (Objects.requireNonNull(file.getContentType()).contains("image")) {
            byte[] content = file.getBytes();
            String encodedString = java.util.Base64.getEncoder().encodeToString(content);
            String login = "anonymous";
            if(SecurityUtils.getCurrentUserLogin().isPresent()) {
                login = SecurityUtils.getCurrentUserLogin().get();
                Optional<Object> optionalUserDTO = service.getUserWithAuthoritiesByLogin(login).map(UserDTO::new);
                UserDTO user = (UserDTO) optionalUserDTO.get();
                user.setImageUrl(encodedString);
                service.updateUser(user);
        }
        }else{
            return new ResponseEntity<>("Invalid file.", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("File Uploaded Successfully, refresh the page to update photo.",HttpStatus.OK);
    }

    @GetMapping("/userManager")
    public ModelAndView userManager(Pageable pageable) throws URISyntaxException {
        ModelAndView mv = new ModelAndView("views/userManager");
        String login = "anonymous";
        if(SecurityUtils.getCurrentUserLogin().isPresent()) {
            login = SecurityUtils.getCurrentUserLogin().get();
            Optional<UserDTO> optionalUserDTO = service.getUserWithAuthoritiesByLogin(login).map(UserDTO::new);
            optionalUserDTO.ifPresent(userDTO -> mv.addObject("user", userDTO));
        }
        mv.addObject("users", service.getAllManagedUsers(pageable));
        return mv;
    }

    @PostMapping("/userManager")
    public ModelAndView userManagerPost(Pageable pageable, RedirectAttributes redirectAttributes, @RequestBody MultiValueMap<String,String> formData) throws URISyntaxException {
        ModelAndView mv = new ModelAndView("views/userManager");
        Map<String,String> singleFormData = formData.toSingleValueMap();
        String login = "anonymous";
        if(singleFormData.containsKey("system")){
            String e = "";
        }
        login = SecurityUtils.getCurrentUserLogin().get();
        UserDTO currentUser = service.getUserWithAuthoritiesByLogin(login).map(UserDTO::new).get();
        Page<UserDTO> users = service.getAllManagedUsers(pageable);
        for (UserDTO user: users) {
            if(!user.getLogin().equals(currentUser.getLogin())){
                Set<String> currentUserAuthorities = user.getAuthorities();
                if(!singleFormData.containsKey(user.getLogin())){
                    currentUserAuthorities.remove("ROLE_ADMIN");
                    user.setAuthorities(currentUserAuthorities);
                }
                else{
                    currentUserAuthorities.add("ROLE_ADMIN");
                    user.setAuthorities(currentUserAuthorities);
                }
                service.updateUser(user);
            }
        }
        mv.addObject("user",currentUser);
        mv.addObject("users", users);
        return mv;
    }

    @GetMapping("/quizinfo/{number}")
    public ModelAndView quizInfo(Pageable pageable, @PathVariable long number) throws URISyntaxException {
        ModelAndView mv = new ModelAndView("views/singularQuiz");
        String login = "anonymous";
        if(SecurityUtils.getCurrentUserLogin().isPresent()) {
            login = SecurityUtils.getCurrentUserLogin().get();
            Optional<UserDTO> optionalUserDTO = service.getUserWithAuthoritiesByLogin(login).map(UserDTO::new);
            optionalUserDTO.ifPresent(userDTO -> mv.addObject("user", userDTO));
        }
        Quiz quiz = quizService.findOneQuiz(number).get();
        mv.addObject("quiz", quiz);
        return mv;
    }

    @GetMapping("/quizList/{subject}")
    public ModelAndView quizListBySubject(Pageable pageable, @PathVariable long subject) throws URISyntaxException {
        ModelAndView mv = new ModelAndView("views/quizBySubjectList");
        String login = "anonymous";
        if(SecurityUtils.getCurrentUserLogin().isPresent()) {
            login = SecurityUtils.getCurrentUserLogin().get();
            Optional<UserDTO> optionalUserDTO = service.getUserWithAuthoritiesByLogin(login).map(UserDTO::new);
            optionalUserDTO.ifPresent(userDTO -> mv.addObject("user", userDTO));
        }
        Page<QuizDTO> quizzes = quizService.findAllBySubject(pageable, subject);

        mv.addObject("pages", (quizzes.getTotalElements()/5)-1);
        mv.addObject("quizzes", quizzes.getContent());
        mv.addObject("subject",quizzes.getContent().get(0).getSubjectSubjectName());
        return mv;
    }

    @PostMapping("/quizinfo/{number}")
        public RedirectView save(@RequestBody MultiValueMap<String, String> formData, @PathVariable int number, final RedirectAttributes redirectAttributes) throws URISyntaxException {
            Integer grade = quizService.saveResponse(formData, number);
            redirectAttributes.addFlashAttribute("message", grade);
            return new RedirectView("/oqs");
    }

    @GetMapping("/logoff")
    public ModelAndView logoutSession(HttpSession session)  {
        session.removeAttribute("jwt");
        return new ModelAndView("views/login");
    }

    @GetMapping("/statistics")
    public ModelAndView getStatistics()  {
        ModelAndView mv = new ModelAndView("views/statistics");
        String login = "anonymous";
        if(SecurityUtils.getCurrentUserLogin().isPresent()) {
            login = SecurityUtils.getCurrentUserLogin().get();
            Optional<UserDTO> optionalUserDTO = service.getUserWithAuthoritiesByLogin(login).map(UserDTO::new);
            optionalUserDTO.ifPresent(userDTO -> mv.addObject("user", userDTO));
        }
        Map<String,String> map = subjectService.TotalSumBySubjectToChart();
        List<String> allSubjects = new ArrayList<>();
        List<String> subjectCounter = new ArrayList<>();
        for (Map.Entry<String,String> entry : map.entrySet()) {
            allSubjects.add(entry.getKey());
            subjectCounter.add(entry.getValue());
        }
        Map<String,String> answerMap = quizService.TotalSumByQuizAnswersToChart();
        List<String> allQuizSubjects = new ArrayList<>();
        List<String> answersCounter = new ArrayList<>();
        for (Map.Entry<String,String> entry : answerMap.entrySet()) {
            allQuizSubjects.add(entry.getKey());
            answersCounter.add(entry.getValue());
        }
        mv.addObject("allSubjects", allSubjects);
        mv.addObject("subjectCounter", subjectCounter);
        mv.addObject("allQuizSubjects", allQuizSubjects);
        mv.addObject("answersCounter", answersCounter);
        return mv;
    }
}

