package com.carlos.oqs.web.mvc;
import com.carlos.oqs.domain.*;
import com.carlos.oqs.security.SecurityUtils;
import com.carlos.oqs.service.QuizService;
import com.carlos.oqs.service.UserService;
import com.carlos.oqs.service.SubjectService;
import com.carlos.oqs.service.AuditEventService;
import com.carlos.oqs.service.dto.QuizDTO;
import com.carlos.oqs.service.dto.SubjectDTO;
import com.carlos.oqs.service.dto.UserDTO;
import com.carlos.oqs.web.rest.QuizResource;
import com.carlos.oqs.web.rest.UserResource;
import io.swagger.models.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.lang.reflect.Array;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Controller
public class StartUserController {

    @Autowired
    private UserService service;

    @Autowired
    private QuizService quizService;

    @Autowired
    private AuditEventService auditEventService;

    @Autowired
    private SubjectService subjectService;

    @GetMapping("/gateway")
    public ModelAndView regularAccess(Pageable pageable) throws URISyntaxException {
        ModelAndView mv = new ModelAndView("views/userInfo");
        String login = "anonymous";
        if(SecurityUtils.getCurrentUserLogin().isPresent()) {
            login = SecurityUtils.getCurrentUserLogin().get();
            Optional<UserDTO> optionalUserDTO = service.getUserWithAuthoritiesByLogin(login).map(UserDTO::new);
            optionalUserDTO.ifPresent(userDTO -> mv.addObject("user", userDTO));
        }
        mv.addObject("quizzes", quizService.findAll(pageable).getContent());
        mv.addObject("grades", quizService.AnsweredQuizGetter(pageable));
        mv.addObject("auditInfo", auditEventService.findByPrincipal(login));
        mv.addObject("correctAnswers", quizService.CorrectlyAnsweredQuizzes());
        mv.addObject("wrongAnswers", quizService.WronglyAnsweredQuizzes());
        mv.addObject("allAnswers", quizService.AllAnsweredQuizzes());
        mv.addObject("allSubjects", subjectService.findAll());
        mv.addObject("subjectCounter", subjectService.TotalSumBySubject());
        mv.addObject("userSubjectCounter", subjectService.UsersSumBySubject());
        return mv;
    }


}
