
package com.carlos.oqs.web.mvc;
    import com.carlos.oqs.domain.*;
    import com.carlos.oqs.security.SecurityUtils;
    import com.carlos.oqs.service.QuizService;
    import com.carlos.oqs.service.UserService;
    import com.carlos.oqs.service.SubjectService;
    import com.carlos.oqs.service.AuditEventService;
    import com.carlos.oqs.service.dto.*;
    import com.carlos.oqs.service.mapper.SubjectMapper;
    import com.carlos.oqs.web.rest.QuizResource;
    import com.carlos.oqs.web.rest.UserResource;
    import io.swagger.models.Model;
    import org.springframework.beans.BeanUtils;
    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.data.domain.Page;
    import org.springframework.data.domain.PageRequest;
    import org.springframework.data.domain.Pageable;
    import org.springframework.stereotype.Controller;
    import org.springframework.util.MultiValueMap;
    import org.springframework.web.bind.annotation.*;
    import org.springframework.web.servlet.ModelAndView;
    import org.springframework.web.servlet.mvc.support.RedirectAttributes;
    import org.springframework.web.servlet.view.RedirectView;

    import java.lang.reflect.Array;
    import java.net.URISyntaxException;
    import java.util.*;

@Controller
public class CreateQuizController {

    @Autowired
    private UserService service;

    @Autowired
    private QuizService quizService;

    @Autowired
    private AuditEventService auditEventService;

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private SubjectMapper subjectMapper;

    @GetMapping("/createQuiz")
    public ModelAndView regularAccess(Pageable pageable) throws URISyntaxException {
        ModelAndView mv = new ModelAndView("views/createQuiz");
        String login = "anonymous";
        if(SecurityUtils.getCurrentUserLogin().isPresent()) {
            login = SecurityUtils.getCurrentUserLogin().get();
            Optional<UserDTO> optionalUserDTO = service.getUserWithAuthoritiesByLogin(login).map(UserDTO::new);
            optionalUserDTO.ifPresent(userDTO -> mv.addObject("user", userDTO));
            mv.addObject("subjects",subjectService.findAll());
        }
        return mv;
    }


    @GetMapping("/updatableQuizzes")
    public ModelAndView updatableQuizzes(Pageable pageable) throws URISyntaxException {
        ModelAndView mv = new ModelAndView("views/updatableQuizzes");
        String login = "anonymous";
        if(SecurityUtils.getCurrentUserLogin().isPresent()) {
            login = SecurityUtils.getCurrentUserLogin().get();
            Optional<UserDTO> optionalUserDTO = service.getUserWithAuthoritiesByLogin(login).map(UserDTO::new);
            optionalUserDTO.ifPresent(userDTO -> mv.addObject("user", userDTO));
        }
        List<QuizDTO> newList = quizService.findAllUpdatable();
        mv.addObject("quizzes", newList);
        return mv;
    }


    @GetMapping("/myquiz/{number}")
    public ModelAndView regularAccess(Pageable pageable, @PathVariable long number, RedirectAttributes redirectAttributes) throws URISyntaxException {
        ModelAndView mv = new ModelAndView("views/myQuiz");
        String login = "anonymous";
        if(SecurityUtils.getCurrentUserLogin().isPresent()) {
            login = SecurityUtils.getCurrentUserLogin().get();
            Optional<UserDTO> optionalUserDTO = service.getUserWithAuthoritiesByLogin(login).map(UserDTO::new);
            optionalUserDTO.ifPresent(userDTO -> mv.addObject("user", userDTO));
        }
        if(!quizService.findOneQuiz(number).isPresent()){
            redirectAttributes.addFlashAttribute("The selected quiz does not exist!");
            return new ModelAndView("redirect:/updatableQuizzes");
        }
        Quiz quiz = quizService.findOneQuiz(number).get();
        mv.addObject("quiz",quiz);
        return mv;
    }

    @PostMapping("/myquiz/{number}")
    public ModelAndView regularPublication(Pageable pageable, @PathVariable long number, RedirectAttributes redirectAttributes, @RequestBody MultiValueMap<String,String> formData) throws URISyntaxException{
        ModelAndView mv = new ModelAndView("views/myquiz/{"+number+"}");
        String login = "anonymous";
        if(SecurityUtils.getCurrentUserLogin().isPresent()) {
            login = SecurityUtils.getCurrentUserLogin().get();
            Optional<UserDTO> optionalUserDTO = service.getUserWithAuthoritiesByLogin(login).map(UserDTO::new);
            optionalUserDTO.ifPresent(userDTO -> mv.addObject("user", userDTO));
        }
        if(!quizService.findOne(number).isPresent()){
            redirectAttributes.addFlashAttribute("The selected quiz was not updated, confirm that it exists and try again!");
            return new ModelAndView("views/404");
        }
        quizService.UpdateQuizForm(formData, number);
        return mv;
    }


    @PostMapping("/createQuiz")
    public RedirectView createQuizPage(@RequestBody MultiValueMap<String,String> formData) throws URISyntaxException {
        RedirectView mv = new RedirectView("/oqs?page=0&size=5");
        quizService.SaveQuizForm(formData);
        return mv;
    }

    @PostMapping("/addSubject")
    public RedirectView addSubject(@RequestParam("file") String file, RedirectAttributes redirectAttributes){
        RedirectView rv = new RedirectView("/createQuiz");
        if(subjectService.AddNewSubject(file)){
            redirectAttributes.addFlashAttribute("resultSubjectSubmission","error");
        }
        else{
            redirectAttributes.addFlashAttribute("resultSubjectSubmission","success");
        }
        return rv;
    }

}
