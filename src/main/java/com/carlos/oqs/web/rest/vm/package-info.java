/**
 * View Models used by Spring MVC REST controllers.
 */
package com.carlos.oqs.web.rest.vm;
