package com.carlos.oqs.web.rest;

import com.carlos.oqs.domain.UserQuizz;
import com.carlos.oqs.repository.UserQuizzRepository;
import com.carlos.oqs.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional; 
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.carlos.oqs.domain.UserQuizz}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class UserQuizzResource {

    private final Logger log = LoggerFactory.getLogger(UserQuizzResource.class);

    private static final String ENTITY_NAME = "userQuizz";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserQuizzRepository userQuizzRepository;

    public UserQuizzResource(UserQuizzRepository userQuizzRepository) {
        this.userQuizzRepository = userQuizzRepository;
    }

    /**
     * {@code POST  /user-quizzes} : Create a new userQuizz.
     *
     * @param userQuizz the userQuizz to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new userQuizz, or with status {@code 400 (Bad Request)} if the userQuizz has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/user-quizzes")
    public ResponseEntity<UserQuizz> createUserQuizz(@RequestBody UserQuizz userQuizz) throws URISyntaxException {
        log.debug("REST request to save UserQuizz : {}", userQuizz);
        if (userQuizz.getId() != null) {
            throw new BadRequestAlertException("A new userQuizz cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserQuizz result = userQuizzRepository.save(userQuizz);
        return ResponseEntity.created(new URI("/api/user-quizzes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /user-quizzes} : Updates an existing userQuizz.
     *
     * @param userQuizz the userQuizz to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userQuizz,
     * or with status {@code 400 (Bad Request)} if the userQuizz is not valid,
     * or with status {@code 500 (Internal Server Error)} if the userQuizz couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/user-quizzes")
    public ResponseEntity<UserQuizz> updateUserQuizz(@RequestBody UserQuizz userQuizz) throws URISyntaxException {
        log.debug("REST request to update UserQuizz : {}", userQuizz);
        if (userQuizz.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UserQuizz result = userQuizzRepository.save(userQuizz);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, userQuizz.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /user-quizzes} : get all the userQuizzes.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of userQuizzes in body.
     */
    @GetMapping("/user-quizzes")
    public List<UserQuizz> getAllUserQuizzes() {
        log.debug("REST request to get all UserQuizzes");
        return userQuizzRepository.findAll();
    }

    /**
     * {@code GET  /user-quizzes/:id} : get the "id" userQuizz.
     *
     * @param id the id of the userQuizz to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the userQuizz, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/user-quizzes/{id}")
    public ResponseEntity<UserQuizz> getUserQuizz(@PathVariable Long id) {
        log.debug("REST request to get UserQuizz : {}", id);
        Optional<UserQuizz> userQuizz = userQuizzRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(userQuizz);
    }

    /**
     * {@code DELETE  /user-quizzes/:id} : delete the "id" userQuizz.
     *
     * @param id the id of the userQuizz to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/user-quizzes/{id}")
    public ResponseEntity<Void> deleteUserQuizz(@PathVariable Long id) {
        log.debug("REST request to delete UserQuizz : {}", id);
        userQuizzRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
