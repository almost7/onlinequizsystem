package com.carlos.oqs.config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@ComponentScan(basePackages = { "com.carlos.oqs.web.mvc" })
public class pathControl extends WebMvcConfigurerAdapter {
    //https://memorynotfound.com/adding-static-resources-css-javascript-images-thymeleaf/
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(
            "/plugins/**",
            "/dist/**")
            .addResourceLocations(
                "classpath:/plugins/",
                "classpath:/dist/");
    }

}




