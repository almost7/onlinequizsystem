package com.carlos.oqs.repository;
import com.carlos.oqs.domain.UserQuizz;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the UserQuizz entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserQuizzRepository extends JpaRepository<UserQuizz, Long> {

    @Query("select userQuizz from UserQuizz userQuizz where userQuizz.user.login = ?#{principal.username}")
    List<UserQuizz> findByUserIsCurrentUser();

    @Query("select count(*) from UserQuizz userQuizz where userQuizz.user.login = ?#{principal.username} and userQuizz.quiz.subject.id = :subjectId")
    Long findByUserIsCurrentUserBySubject(@Param("subjectId") Long SubjectId);

}
