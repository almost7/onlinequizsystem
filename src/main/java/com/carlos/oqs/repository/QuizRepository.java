package com.carlos.oqs.repository;
import com.carlos.oqs.domain.Quiz;
import com.carlos.oqs.service.dto.QuizDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;


/**
 * Spring Data  repository for the Quiz entity.
 */
@SuppressWarnings("unused")
@Repository
public interface QuizRepository extends JpaRepository<Quiz, Long> {

    // @Query("SELECT quiz FROM Quiz quiz WHERE quiz.userQuizz.user.login = ?#{principal.username}")

    @Query("SELECT userquiz.quiz FROM UserQuizz userquiz WHERE userquiz.user.login = ?#{principal.username}")
    Page<Quiz> findQuizByUser(Pageable pageable);

    Quiz findById(long id);

    @Query("Select count(*) from Quiz quiz where quiz.subject.id = :subjectId ")
    Long countAllBySubjectId(@Param("subjectId") Long subjectId);

    @Query("select count(*) from UserQuizz userQuizz where userQuizz.quiz.id = :quizId")
    Long countAllByAnsweredQuizId(@Param("quizId") Long quizId);

    @Query("select quiz from Quiz quiz where (Select count(*) from Question question where question.quiz.id = quiz.id) >= 10")
    Page<Quiz> findQuizzesAboveTen(Pageable pageable);

    @Query("Select quiz from Quiz quiz where quiz.subject.id = :subjectId ")
    Page<Quiz> findAllBySubjectId(Pageable pageable, @Param("subjectId") Long subjectId);

    @Query("Select quiz from Quiz quiz where lower(quiz.subject.subjectName) like %:subjectName% and (Select count(*) from Question question where question.quiz.id = quiz.id) >= 10")
    Page<Quiz> findAllBySubjectName(Pageable pageable, @Param("subjectName") String subjectName);
}
