package com.carlos.oqs.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Quiz.
 */
@Entity
@Table(name = "quiz")
public class Quiz implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "quiz_name")
    private String quizName;

    @Column(name = "min_grade")
    private Integer minGrade;

    @Column(name = "avg_grade")
    private Integer avgGrade;

    @OneToMany(mappedBy = "quiz", fetch=FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<Question> questions = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties("quizzes")
    private Subject subject;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuizName() {
        return quizName;
    }

    public Quiz quizName(String quizName) {
        this.quizName = quizName;
        return this;
    }

    public void setQuizName(String quizName) {
        this.quizName = quizName;
    }

    public Integer getMinGrade() {
        return minGrade;
    }

    public Quiz minGrade(Integer minGrade) {
        this.minGrade = minGrade;
        return this;
    }

    public void setMinGrade(Integer minGrade) {
        this.minGrade = minGrade;
    }

    public Integer getAvgGrade() {
        return avgGrade;
    }

    public Quiz avgGrade(Integer avgGrade) {
        this.avgGrade = avgGrade;
        return this;
    }

    public void setAvgGrade(Integer avgGrade) {
        this.avgGrade = avgGrade;
    }

    public Set<Question> getQuestions() {
        return questions;
    }

    public Quiz questions(Set<Question> questions) {
        this.questions = questions;
        return this;
    }

    public Quiz addQuestion(Question question) {
        this.questions.add(question);
        question.setQuiz(this);
        return this;
    }

    public Quiz removeQuestion(Question question) {
        this.questions.remove(question);
        question.setQuiz(null);
        return this;
    }

    public void setQuestions(Set<Question> questions) {
        this.questions = questions;
    }

    public Subject getSubject() {
        return subject;
    }

    public Quiz subject(Subject subject) {
        this.subject = subject;
        return this;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Quiz)) {
            return false;
        }
        return id != null && id.equals(((Quiz) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Quiz{" +
            "id=" + getId() +
            ", quizName='" + getQuizName() + "'" +
            ", minGrade=" + getMinGrade() +
            ", avgGrade=" + getAvgGrade() +
            "}";
    }
}
