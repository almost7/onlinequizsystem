package com.carlos.oqs.service.dto;

import java.io.Serializable;
import java.util.*;

/**
 * A DTO for the {@link com.carlos.oqs.domain.Question} entity.
 */
public class QuestionDTO implements Serializable {

    private Long id;

    private String questionTitle;


    private Long quizId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestionTitle() {
        return questionTitle;
    }

    public void setQuestionTitle(String questionTitle) {
        this.questionTitle = questionTitle;
    }

    public Long getQuizId() {
        return quizId;
    }

    public void setQuizId(Long quizId) {
        this.quizId = quizId;
    }

    @Override
    public String toString() {
        return "QuestionDTO{" +
            "id=" + id +
            ", questionTitle='" + questionTitle + '\'' +
            ", quizId=" + quizId +
            ", answers=" + answers +
            '}';
    }

    public Set<AnswerDTO> getAnswers() {
        return answers;
    }

    public void setAnswers(Set<AnswerDTO> answers) {
        this.answers = answers;
    }

    public Set<AnswerDTO> answers;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        QuestionDTO questionDTO = (QuestionDTO) o;
        if (questionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), questionDTO.getId());
    }


    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

}
