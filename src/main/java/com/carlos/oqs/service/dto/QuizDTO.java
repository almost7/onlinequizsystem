package com.carlos.oqs.service.dto;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the {@link com.carlos.oqs.domain.Quiz} entity.
 */
public class QuizDTO implements Serializable {

    private Long id;

    private String quizName;

//    private String quizDescription;


    private Long subjectId;

    private String subjectSubjectName;

    private Integer minGrade;

    private Integer avgGrade;

    private Long userId;

    public Integer getMinGrade() {return minGrade;}

    public void setMinGrade(Integer minGrade) { this.minGrade = minGrade;}

    public Integer getAvgGrade() {return avgGrade;}

    public void setAvgGrade(Integer avgGrade) { this.avgGrade = avgGrade;}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuizName() {
        return quizName;
    }

    public void setQuizName(String quizName) {
        this.quizName = quizName;
    }

    @Override
    public String toString() {
        return "QuizDTO{" +
            "id=" + id +
            ", quizName='" + quizName + '\'' +
            ", subjectId=" + subjectId +
            ", subjectSubjectName='" + subjectSubjectName + '\'' +
            ", userId=" + userId +
            ", questionDTOList=" + questions +
            '}';
    }


    Set<QuestionDTO> questions;
//    public String getQuizDescription() {

    public Set<QuestionDTO> getQuestions() {
        return questions;
    }

    public void setQuestions(Set<QuestionDTO> questions) {
        this.questions = questions;
    }
//        return quizDescription;
//    }
//
//    public void setQuizDescription(String quizDescription) {
//        this.quizDescription = quizDescription;
//    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectSubjectName() {
        return subjectSubjectName;
    }

    public void setSubjectSubjectName(String subjectSubjectName) {
        this.subjectSubjectName = subjectSubjectName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        QuizDTO quizDTO = (QuizDTO) o;
        if (quizDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), quizDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

}
