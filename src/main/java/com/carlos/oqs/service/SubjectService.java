package com.carlos.oqs.service;

import com.carlos.oqs.domain.Quiz;
import com.carlos.oqs.domain.Subject;
import com.carlos.oqs.domain.UserQuizz;
import com.carlos.oqs.repository.QuizRepository;
import com.carlos.oqs.repository.SubjectRepository;
import com.carlos.oqs.repository.UserQuizzRepository;
import com.carlos.oqs.service.dto.QuizDTO;
import com.carlos.oqs.service.dto.SubjectDTO;
import com.carlos.oqs.service.mapper.SubjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;
import com.carlos.oqs.service.QuizService;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


/**
 * Service Implementation for managing {@link Subject}.
 */
@Service
@Transactional
public class SubjectService {

    private final Logger log = LoggerFactory.getLogger(SubjectService.class);

    private final SubjectRepository subjectRepository;

    private final SubjectMapper subjectMapper;
    private final UserQuizzRepository userQuizzRepository;
    private final QuizRepository quizRepository;

    public SubjectService(SubjectRepository subjectRepository, SubjectMapper subjectMapper, UserQuizzRepository userQuizzRepository, QuizRepository quizRepository) {
        this.subjectRepository = subjectRepository;
        this.subjectMapper = subjectMapper;
        this.userQuizzRepository = userQuizzRepository;
        this.quizRepository = quizRepository;
    }


    /**
     * Save a subject.
     *
     * @param subjectDTO the entity to save.
     * @return the persisted entity.
     */
    public SubjectDTO save(SubjectDTO subjectDTO) {
        log.debug("Request to save Subject : {}", subjectDTO);
        Subject subject = subjectMapper.toEntity(subjectDTO);
        subject = subjectRepository.save(subject);
        return subjectMapper.toDto(subject);
    }

    /**
     * Get all the subjects.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<SubjectDTO> findAll() {
        log.debug("Request to get all Subjects");
        return subjectRepository.findAll().stream()
            .map(subjectMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one subject by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<SubjectDTO> findOne(Long id) {
        log.debug("Request to get Subject : {}", id);
        return subjectRepository.findById(id)
            .map(subjectMapper::toDto);
    }

    /**
     * Delete the subject by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Subject : {}", id);
        subjectRepository.deleteById(id);
    }

    @Autowired
    private QuizService quizService;

    @Transactional(readOnly = true)
    public List<Integer> AnsweredQuizzesBySubject(Pageable pageable) {
        List<Subject> subjects = subjectRepository.findAll();
        List<UserQuizz> answeredList = userQuizzRepository.findByUserIsCurrentUser();
        List<Integer> countBySubject = new ArrayList<>();
        for (Subject subject : subjects) {
            int tempValue = 0;
            for (UserQuizz answered : answeredList) {
                if (answered.getQuiz().getSubject().getSubjectName().equals(subject.getSubjectName())) {
                    tempValue++;
                }
            }
            countBySubject.add(tempValue);
        }
        return countBySubject;
    }

    @Transactional(readOnly = true)
    public List<Long> TotalSumBySubject() {
        List<Subject> subjects = subjectRepository.findAll();
        List<Long> countBySubject = new ArrayList<>();
        for (Subject subject : subjects) {
                countBySubject.add(quizRepository.countAllBySubjectId(subject.getId()));
        }
        return countBySubject;
    }

    @Transactional(readOnly = true)
    public Map<String,String> TotalSumBySubjectToChart() {
        List<Subject> subjects = subjectRepository.findAll();
        Map<String,String> countBySubject = new HashMap<>();
        for (Subject subject : subjects) {
            if(quizRepository.countAllBySubjectId(subject.getId()) > 0) {
                countBySubject.put(subject.getSubjectName(), quizRepository.countAllBySubjectId(subject.getId()).toString());
            }
        }
        return countBySubject;
    }



    @Transactional(readOnly = true)
    public List<Long> UsersSumBySubject() {
        List<Subject> subjects = subjectRepository.findAll();
        List<Long> countBySubject = new ArrayList<>();
        for (Subject subject : subjects) {
            countBySubject.add(userQuizzRepository.findByUserIsCurrentUserBySubject(subject.getId()));
        }
        return countBySubject;
    }

    @Transactional(readOnly = true)
    public boolean AddNewSubject(String newSubject) {
        List<Subject> subjects = subjectRepository.findAll();
        boolean checker = false;
        for (Subject subject: subjects) {
            if(subject.getSubjectName().toLowerCase().equals(newSubject.toLowerCase())){
                checker = true;
                break;
            }
        }
        if(!checker){
            Subject subject = new Subject();
            subject.subjectName(newSubject);
            subjectRepository.save(subject);
        }
        return checker;
    }
}
