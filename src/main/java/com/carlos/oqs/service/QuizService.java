package com.carlos.oqs.service;

import com.carlos.oqs.domain.*;
import com.carlos.oqs.repository.AnswerRepository;
import com.carlos.oqs.repository.SubjectRepository;
import com.carlos.oqs.repository.UserQuizzRepository;
import com.carlos.oqs.repository.QuizRepository;
import com.carlos.oqs.security.SecurityUtils;
import com.carlos.oqs.service.dto.QuizDTO;
import com.carlos.oqs.service.dto.SubjectDTO;
import com.carlos.oqs.service.dto.UserDTO;
import com.carlos.oqs.service.mapper.QuizMapper;
import com.carlos.oqs.service.mapper.SubjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Service Implementation for managing {@link Quiz}.
 */
@Service
@Transactional
public class QuizService {

    private final Logger log = LoggerFactory.getLogger(QuizService.class);

    private final QuizRepository quizRepository;
    private final UserQuizzRepository userQuizzRepository;
    private final QuizMapper quizMapper;
    private final AnswerRepository answerRepository;
    private final UserService userService;
    private final SubjectRepository subjectRepository;
    private final SubjectMapper subjectMapper;

    public QuizService(QuizRepository quizRepository, SubjectMapper subjectMapper, QuizMapper quizMapper, UserQuizzRepository userQuizzRepository, AnswerRepository answerRepository, UserService userService, SubjectRepository subjectRepository) {
        this.quizRepository = quizRepository;
        this.quizMapper = quizMapper;
        this.userQuizzRepository = userQuizzRepository;
        this.answerRepository = answerRepository;
        this.userService = userService;
        this.subjectRepository = subjectRepository;
        this.subjectMapper = subjectMapper;
    }

    /**
     * Save a quiz.
     *
     * @param quizDTO the entity to save.
     * @return the persisted entity.
     */
    public QuizDTO save(QuizDTO quizDTO) {
        log.debug("Request to save Quiz : {}", quizDTO);
        Quiz quiz = quizMapper.toEntity(quizDTO);
        quiz = quizRepository.save(quiz);
        return quizMapper.toDto(quiz);
    }

    /**
     * Get all the quizzes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<QuizDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Quizzes");
        return quizRepository.findAll(pageable)
            .map(quizMapper::toDto);
    }


    @Transactional(readOnly = true)
    public Page<QuizDTO> findAllWithQuestions(Pageable pageable) {
        log.debug("Request to get all Quizzes above 10 questions");
        Page<Quiz> quizzesAboveTen = quizRepository.findQuizzesAboveTen(pageable);
        return quizzesAboveTen
            .map(quizMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Map<String,String> TotalSumByQuizAnswersToChart() {
        List<Quiz> quizzes = quizRepository.findAll();
        Map<String,String> countBySubject = new HashMap<>();
        for (Quiz quiz : quizzes) {
            if(quizRepository.countAllByAnsweredQuizId(quiz.getId()) > 0) {
                countBySubject.put(quiz.getQuizName(), quizRepository.countAllByAnsweredQuizId(quiz.getId()).toString());
            }
        }
        return countBySubject;
    }


    @Transactional(readOnly = true)
    public List<QuizDTO> findAllUpdatable() {
        log.debug("Request to get all Quizzes bellow 10 questions");
        List<Quiz> quizzes = quizRepository.findAll();
        List<QuizDTO> fullList = quizMapper.toDto(quizzes);
        List<QuizDTO> finalList = new ArrayList<>();
        for (QuizDTO quiz: fullList) {
            if(quiz.getQuestions().size() < 10){
                finalList.add(quiz);
            }
        }
        return finalList;
    }

    /**
     * Get one quiz by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<QuizDTO> findOne(Long id) {
        log.debug("Request to get Quiz : {}", id);
        return quizRepository.findById(id)
            .map(quizMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<QuizDTO> findOneByUser(Pageable pageable) {
        log.debug("Request to get Quiz by User");
        return quizRepository.findQuizByUser(pageable).map(quizMapper::toDto);
    }

    /**
     * Delete the quiz by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Quiz : {}", id);
        quizRepository.deleteById(id);
    }

    @Transactional(readOnly = true)
    public List<Integer> AnsweredQuizGetter(Pageable pageable) {
        log.debug("Request to get all Quizzes already answered by logged user");
        List<QuizDTO> allQuizzesList = this.findAll(pageable).getContent();
        List<Integer> grades = new ArrayList<>();
        boolean isThere = false;
        if(this.findOneByUser(pageable) != null){
            List<UserQuizz> answeredList = userQuizzRepository.findByUserIsCurrentUser();
            for (QuizDTO quiz: allQuizzesList ) {
                int tempValue = 0;
                isThere = false;
                for (int i = 0; i< answeredList.size(); i++){
                    if(quiz.getId().equals(answeredList.get(i).getQuiz().getId())){
                        isThere = true;
                        tempValue = answeredList.get(i).getGrade();
                    }
                }
                if(isThere){
                    grades.add(tempValue);
                }
                else{
                    grades.add(101);
                }
            }
        }
        return grades;
    }

    @Transactional(readOnly = true)
    public void SaveQuizForm(@RequestBody MultiValueMap<String,String> formData) {
        Quiz quiz = new Quiz();
        quiz.setQuizName(formData.getFirst("quizTitle"));
        quiz.setMinGrade(Integer.parseInt(Objects.requireNonNull(formData.getFirst("minGrade"))));
        quiz.setAvgGrade(Integer.parseInt(Objects.requireNonNull(formData.getFirst("avgGrade"))));
        log.debug("requesting subjectRepository to return subject by ID {}",formData.getFirst("subjectSelector"));
        Optional<Subject> subjectOpt = subjectRepository.findById(Long.parseLong(formData.getFirst("subjectSelector")));
        Subject subject;
        if(subjectOpt.isPresent()){
            subject = subjectRepository.findById(Long.parseLong(formData.getFirst("subjectSelector"))).get();
        }
        else{
            subject = subjectRepository.findById(15L).get();
        }
        quiz.setSubject(subject);

        Set<Question> questionSet = new HashSet<>();
        List<String> rightAnswers = formData.get("rightAnswer[]");
        List<String> firstWrongAnswers = formData.get("firstWrongAnswer[]");
        List<String> secondWrongAnswers = formData.get("secondWrongAnswer[]");
        List<String> thirdWrongAnswers = formData.get("thirdWrongAnswer[]");
        List<String> questionTitles = formData.get("questionTitle[]");
        List<String> questionDescriptions = formData.get("questionDescription[]");
        boolean check = false;
        log.debug("checking if formData holds any questions filled");
        for (String value: questionTitles) {
            if (!value.equals("")) {
                check = true;
                break;
            }
        }
        if(check) {
            for (int i = 0; i < questionTitles.size(); i++) {
                if (!questionTitles.get(i).equals("")) {
                    Question question = new Question();
                    Set<Answer> set = new HashSet<>();
                    question.setQuestionTitle(questionTitles.get(i));
                    question.setQuestionDescription(questionDescriptions.get(i));
                    Answer rightAnswer = new Answer();
                    rightAnswer.setAnswerName(rightAnswers.get(i));
                    rightAnswer.setCorrectAnswer(true);
                    rightAnswer.setQuestion(question);
                    set.add(rightAnswer);

                    Answer wrongAnswer1 = new Answer();
                    wrongAnswer1.setAnswerName(firstWrongAnswers.get(i));
                    wrongAnswer1.setCorrectAnswer(false);
                    wrongAnswer1.setQuestion(question);
                    set.add(wrongAnswer1);

                    Answer wrongAnswer2 = new Answer();
                    wrongAnswer2.setAnswerName(secondWrongAnswers.get(i));
                    wrongAnswer2.setCorrectAnswer(false);
                    wrongAnswer2.setQuestion(question);
                    set.add(wrongAnswer2);

                    Answer wrongAnswer3 = new Answer();
                    wrongAnswer3.setAnswerName(thirdWrongAnswers.get(i));
                    wrongAnswer3.setCorrectAnswer(false);
                    wrongAnswer3.setQuestion(question);
                    set.add(wrongAnswer3);

                    question.setAnswers(set);
                    questionSet.add(question);
                    question.setQuiz(quiz);
                }
            }
            quiz.setQuestions(questionSet);
            log.debug("saving quiz");
            quizRepository.save(quiz);
        }
    }

    @Transactional(readOnly = true)
    public Optional<Quiz> findOneQuiz(Long id) {
        return quizRepository.findById(id);
    }

    public Integer saveResponse(MultiValueMap<String, String> formData, long number) {
        String login = SecurityUtils.getCurrentUserLogin().get();
        User user = userService.getUserWithAuthoritiesByLogin(login).get();
        List<UserQuizz> userQuizList = userQuizzRepository.findByUserIsCurrentUser();
        for (UserQuizz userQuiz: userQuizList) {
            if(userQuiz.getQuiz().getId()==number){
                return 404;
            }
        }
        AtomicInteger grade = new AtomicInteger();
        Map<String,String> singleFormData = formData.toSingleValueMap();
        singleFormData.forEach((k,v) -> grade.addAndGet(getTruth(v)));
        UserQuizz userQuizz = new UserQuizz();

        userQuizz.setUser(user);
        userQuizz.setQuiz(quizRepository.findById(number));
        userQuizz.setGrade(grade.get());
        userQuizzRepository.save(userQuizz);
        return grade.get();
    }

    private int getTruth(String value){
        int grade = 0;
        if(answerRepository.getOne(Long.valueOf(value)).isCorrectAnswer())
            grade = 10;
        return grade;
    }

    public int CorrectlyAnsweredQuizzes(){
        List<UserQuizz> answeredList = userQuizzRepository.findByUserIsCurrentUser();
        int correctlyAnswered = 0;
        for (UserQuizz userquizz: answeredList) {
            if(userquizz.getGrade() >= 50){
                correctlyAnswered++;
            }
        }
        return correctlyAnswered;
    }

    public int WronglyAnsweredQuizzes(){
        List<UserQuizz> answeredList = userQuizzRepository.findByUserIsCurrentUser();
        int wronglyAnswered = 0;
        for (UserQuizz userquizz: answeredList) {
            if(userquizz.getGrade() < 50){
                wronglyAnswered += 1;
            }
        }
        return wronglyAnswered;
    }

    public int AllAnsweredQuizzes(){
        return userQuizzRepository.findByUserIsCurrentUser().size();
    }

    @Transactional(readOnly = true)
    public void UpdateQuizForm(@RequestBody MultiValueMap<String,String> formData, long number) {
        log.debug("requesting quizRepository to return quiz by ID {}",number);
        Quiz quiz = quizRepository.findById(number);
        log.debug("creating Lists for answers and questions from formData received");
        Set<Question> questionSet = new HashSet<>();
        List<String> rightAnswers = formData.get("rightAnswer[]");
        List<String> firstWrongAnswers = formData.get("firstWrongAnswer[]");
        List<String> secondWrongAnswers = formData.get("secondWrongAnswer[]");
        List<String> thirdWrongAnswers = formData.get("thirdWrongAnswer[]");
        List<String> questionTitles = formData.get("questionTitle[]");
        List<String> questionDescriptions = formData.get("questionDescription[]");
        boolean check = false;
        log.debug("checking if formData holds any questions filled");
        for (String value: questionTitles) {
            if (!value.equals("")) {
                check = true;
                break;
            }
        }
        log.debug("Adding answers to questions and questions to quiz");
        if(check) {
            for (int i = 0; i < questionTitles.size(); i++) {
                if (!questionTitles.get(i).equals("")) {
                    Question question = new Question();
                    Set<Answer> set = new HashSet<>();
                    question.setQuestionTitle(questionTitles.get(i));
                    question.setQuestionDescription(questionDescriptions.get(i));
                    Answer rightAnswer = new Answer();
                    rightAnswer.setAnswerName(rightAnswers.get(i));
                    rightAnswer.setCorrectAnswer(true);
                    rightAnswer.setQuestion(question);
                    set.add(rightAnswer);

                    Answer wrongAnswer1 = new Answer();
                    wrongAnswer1.setAnswerName(firstWrongAnswers.get(i));
                    wrongAnswer1.setCorrectAnswer(false);
                    wrongAnswer1.setQuestion(question);
                    set.add(wrongAnswer1);

                    Answer wrongAnswer2 = new Answer();
                    wrongAnswer2.setAnswerName(secondWrongAnswers.get(i));
                    wrongAnswer2.setCorrectAnswer(false);
                    wrongAnswer2.setQuestion(question);
                    set.add(wrongAnswer2);

                    Answer wrongAnswer3 = new Answer();
                    wrongAnswer3.setAnswerName(thirdWrongAnswers.get(i));
                    wrongAnswer3.setCorrectAnswer(false);
                    wrongAnswer3.setQuestion(question);
                    set.add(wrongAnswer3);

                    question.setAnswers(set);
                    questionSet.add(question);
                    question.setQuiz(quiz);
                }
            }
            quiz.setQuestions(questionSet);
            quizRepository.save(quiz);
        }
    }

    @Transactional(readOnly = true)
    public Page<QuizDTO> findAllBySubject(Pageable pageable, long subject) {
        log.debug("requesting quizRepository to return subjects by ID with the ID {}",subject);
        return quizRepository.findAllBySubjectId(pageable, subject).map(quizMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<QuizDTO> findAllBySubjectName(Pageable pageable, String subject) {
        String loweredSubject = subject.toLowerCase();
        log.debug("requesting quizRepository to return subjects by NAME with the NAME {}",subject);
        return quizRepository.findAllBySubjectName(pageable, loweredSubject).map(quizMapper::toDto);
    }

}
