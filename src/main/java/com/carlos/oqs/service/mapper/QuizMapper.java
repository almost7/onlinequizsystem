package com.carlos.oqs.service.mapper;

import com.carlos.oqs.domain.*;
import com.carlos.oqs.service.dto.QuizDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Quiz} and its DTO {@link QuizDTO}.
 */
@Mapper(componentModel = "spring", uses = {SubjectMapper.class, UserMapper.class})
public interface QuizMapper extends EntityMapper<QuizDTO, Quiz> {

    @Mapping(source = "subject.id", target = "subjectId")
    @Mapping(source = "subject.subjectName", target = "subjectSubjectName")
//    @Mapping(source = "user.id", target = "userId")
    QuizDTO toDto(Quiz quiz);
//adding data
//    @Mapping(target = "questions", ignore = true)
    @Mapping(target = "removeQuestion", ignore = true)
    @Mapping(source = "subjectId", target = "subject")
//    @Mapping(source = "userId", target = "user")
    Quiz toEntity(QuizDTO quizDTO);

    default Quiz fromId(Long id) {
        if (id == null) {
            return null;
        }
        Quiz quiz = new Quiz();
        quiz.setId(id);
        return quiz;
    }
}
